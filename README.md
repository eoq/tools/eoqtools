# EOQ Tools

EOQ tools are utilities based on EOQ that can be used stand-alone or as helpers when developing with EOQ. This section describes the main tools coming with EOQ.

## Content
* pyeoqwebserver: A Python-based EOQ server that provides access to the domain via web sockets
* jseoqdeveloperinterface: A web-based raw user interface to EOQ. Requires a web socket server.

# EOQ

[Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modification, change events, and error handling.  

Main Repository: https://gitlab.com/eoq/essentialobjectquery

EOQ user manual: https://gitlab.com/eoq/doc 

